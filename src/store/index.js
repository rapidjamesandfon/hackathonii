import Vue from 'vue'
import Vuex from 'vuex'
import firebase from '@/firebase'
import router from '@/router'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    appTitle: 'MUIC Calendar',
    user: null,
    error: null,
    loading: false,
    instructionBegin: '',
    registration: '',
    paymentDue: ''
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setTodos (state, payload) {
      state.todos = payload
    }
  },
  actions: {
    userSignUp ({commit}, payload) {
      commit('setLoading', true)
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
      .then(firebaseUser => {
        commit('setUser', {email: firebaseUser.email})
        commit('setLoading', false)
        router.push('/home')
      })
      .catch(error => {
        commit('setError', error.message)
        commit('setLoading', false)
      })
    },
    userSignIn ({commit}, payload) {
      commit('setLoading', true)
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
      .then(firebaseUser => {
        commit('setUser', {email: firebaseUser.email})
        commit('setLoading', false)
        commit('setError', null)
        router.push('/admin')
      })
      .catch(error => {
        commit('setError', error.message)
        commit('setLoading', false)
      })
    },
    // getAttri ({commit}, payload) {
    //   firebase.database().ref('AcademicYear/' + payload.academicYear)
    //     .on('value', snapshot => {
    //       commit('setTodos', snapshot.val())
    //       console.log(snapshot.val())
    //     })
    // },
    addEvent ({commit}, payload) {
      var data = {
        description: payload.description,
        startDate: payload.startDate,
        endDate: payload.endDate,
        color: payload.color
      }
      firebase.database().ref('AcademicYear/' + payload.academicYear + '/' + payload.trimester).push(data)
      location.reload()
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', {email: payload.email})
    },
    userSignOut ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
      router.push('/signin')
    }
  },
  getters: {
    isAuthenticated (state) {
      return state.user !== null && state.user !== undefined
    }
  }
})
