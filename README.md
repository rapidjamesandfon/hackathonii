# Welcome to MUIC Academic Calendar

> A Vue.js project with v-calendar Demo: https://hackathonii-eeeb8.firebaseapp.com/

MUIC Academic Calendar is a project that turn a static academic calendar to a dynamic calendar with event detail and countdown on important event.
Where admin could add event by signing in (https://hackathonii-eeeb8.firebaseapp.com/admin).

## Prerequisites
* Vue.js 2.0
* Firebase

## Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn start

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
yarn build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

By Kanokporn Pringpayong and Sorakris Chaladlamsakul

Enjoy

